<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role-id')->nullable();
            $table->integer('parent-id')->nullable();
            $table->string("Student_name");
            $table->string('DOB');
            $table->string('gender');
            $table->string('Blood_Group');
            $table->string('Nationality');
            $table->string('Email');
            $table->text('phone_Number');
            $table->string('religion');
            $table->text('photo');
            $table->string('address');
            $table->string('father_name');
            $table->string('mother_name');
            $table->string('father_phn');
            $table->string('mother_phn');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
