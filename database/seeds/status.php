<?php

use Illuminate\Database\Seeder;

class status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('status')->insert([
        	'name' => 'absent'
        	]);
       DB::table('status')->insert([
        	'name' => 'present'
        	]);
    }
}
