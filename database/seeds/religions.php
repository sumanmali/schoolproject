<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class religions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('religions')->insert([
        	'name' => 'hindu'
        ]);
       DB::table('religions')->insert([
            'name' => 'muslim'
        ]);
       DB::table('religions')->insert([
            'name' => 'islam'
        ]);
       DB::table('religions')->insert([
            'name' => 'christan'
        ]);
       DB::table('religions')->insert([
            'name' => 'buddhist'
        ]);
       DB::table('religions')->insert([
            'name' => 'others'
        ]);
    }
}
