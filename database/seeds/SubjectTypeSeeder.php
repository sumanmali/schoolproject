<?php

use Illuminate\Database\Seeder;
use App\SubjectType;
class SubjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubjectType::insert([
            'id' => 1,
            'name' => 'Compulsory'
            ]);
        SubjectType::insert([
            'id' => 2,
            'name' => 'Selective'
            ]);
    }
}
