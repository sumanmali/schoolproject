<?php

use Illuminate\Database\Seeder;

class ReligionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('religions')->insert([
        	'name' => 'Hindu'
        	]);
        DB::table('religions')->insert([
        	'name' => 'Muslim'
        	]);
        DB::table('religions')->insert([
        	'name' => 'Buddhist'
        	]);
        DB::table('religions')->insert([
        	'name' => 'christian'
        	]);
        DB::table('religions')->insert([
        	'name' => 'Islam'
        	]);
        DB::table('religions')->insert([
        	'name' => 'Others'
        	]);
    }
}
