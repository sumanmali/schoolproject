<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\User;

class UsersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $role_student = Role::where('name', 'student')->first();
        // $role_admin = Role::where('name', 'admin')->first();
        // $role_parent = Role::where('name', 'parent')->first();
        // $role_teacher = Role::where('name', 'teacher')->first();


        $student = new User();
        $student->name = 'student';
        $student->email = 'student@student.com';
        $student->password = bcrypt('student');
        $student->save();
        // $student->roles()->attach($role_student);

        $admin = new User();
        $admin->name = 'admin';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('admin');
        $admin->save();
        // $admin->roles()->attach($role_admin);

        $teacher = new User();
        $teacher->name = 'teacher';
        $teacher->email = 'teacher@teacher.com';
        $teacher->password = bcrypt('teacher');
        $teacher->save();
        // $teacher->roles()->attach($role_teacher);

        $parent = new User();
        $parent->name = 'parent';
        $parent->email = 'parent@parent.com';
        $parent->password = bcrypt('parent');
        $parent->save();
        // $parent->roles()->attach($role_parent);
    }
}
