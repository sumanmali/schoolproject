<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Group extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
        	'group_name' => 'school level'
        ]);
       DB::table('groups')->insert([
            'group_name' => 'science'
        ]);
       DB::table('groups')->insert([
            'group_name' => 'humanities'
        ]);
       DB::table('groups')->insert([
        	'group_name' => 'commerce'
        ]);
       DB::table('groups')->insert([
            'group_name' => 'arts'
        ]);
    }
}
