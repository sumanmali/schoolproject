<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class BloodGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bloodgroups')->insert([
        	'name' => 'A+'
        ]);
       DB::table('bloodgroups')->insert([
            'name' => 'A-'
        ]);
       DB::table('bloodgroups')->insert([
            'name' => 'B+'
        ]);
       DB::table('bloodgroups')->insert([
        	'name' => 'B-'
        ]);
       DB::table('bloodgroups')->insert([
            'name' => 'O+'
        ]);
       DB::table('bloodgroups')->insert([
            'name' => 'O-'
        ]);
       DB::table('bloodgroups')->insert([
        	'name' => 'AB+'
        ]);
       DB::table('bloodgroups')->insert([
            'name' => 'AB-'
        ]);
    }
}
