<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/student','StudentController@index');
Route::get('/student/create','StudentController@create');
Route::post('/student/create','StudentController@store');
Route:: get('/student/edit/{id}','StudentController@edit');
Route:: post('/student/edit/{id}','StudentController@update');
Route:: get('/student/delete/{id}','StudentController@destroy');


Route::get('/parent','GuardianController@index');
Route::get('/parent/create','GuardianController@create');
Route::post('/parent/create','GuardianController@store');
Route:: get('/parent/edit/{id}','GuardianController@edit');
Route:: post('/parent/edit/{id}','GuardianController@update');
Route:: delete('/parent/delete/{id}','GuardianController@destroy');


Route::get('/teacher','Teachercontroller@index');
Route::get('/teacher/create','Teachercontroller@create');
Route::post('/teacher/create','Teachercontroller@store');
Route::get('/teacher/edit/{id}','Teachercontroller@edit');
Route::post('/teacher/edit/{id}','Teachercontroller@update');
Route::delete('/teacher/delete/{id}','Teachercontroller@destroy');



Route::get('/staff','StaffController@index');
Route::get('/staff/create','StaffController@create');
Route::post('/staff/create','StaffController@store');
Route:: get('/staff/edit/{id}','StaffController@edit');
Route:: post('/staff/edit/{id}','StaffController@update');
Route:: get('/staff/delete/{id}','StaffController@destroy');



Route::get('/room','RoomController@index');
Route::get('/room/create','RoomController@create');
Route::post('/room/create','RoomController@store');
Route::get('/room/edit/{id}','RoomController@edit');
Route::post('/room/edit/{id}','RoomController@update');
Route::delete('/room/delete/{id}','RoomController@destroy');


Route::get('/class','AllClassController@index');
Route::get('/class/create','AllClassController@create');
Route::post('/class/create','AllClassController@store');
Route::get('/class/edit/{id}','AllClassController@edit');
Route::post('/class/edit/{id}','AllClassController@update');
Route::delete('/class/delete/{id}','AllClassController@destroy');




Route::get('/section','SectionController@index');
Route::get('/section/create','SectionController@create');
Route::post('/section/create','SectionController@store');
Route::get('/section/edit/{id}','SectionController@edit');
Route::post('/section/edit/{id}','SectionController@update');
Route::delete('/section/delete/{id}','SectionController@destroy');



Route::get('/subjects','SubjectController@index');
Route::get('/subject/create','SubjectController@create');
Route::post('/subject/create','SubjectController@store');
Route::get('/subject/edit/{id}','SubjectController@edit');
Route::post('/subject/edit/{id}','SubjectController@update');
Route::delete('/subject/delete/{id}','SubjectController@destroy');



Route::get('/guardian','GuardianController@index');
Route::get('/guardian/create','GuardianController@create');
Route::post('/guardian/create','GuardianController@store');
Route::get('/guardian/edit/{id}','GuardianController@edit');
Route::post('/guardian/edit/{id}','GuardianController@update');
Route::delete('/guardian/delete/{id}','GuardianController@destroy');



Route::get('/attendence','AttendenceController@index');
Route::get('/attendence/create','AttendenceController@create');
Route::post('/attendence/create','AttendenceController@store');
Route::get('/attendence/edit/{id}','AttendenceController@edit');
Route::post('/attendence/edit/{id}','AttendenceController@update');
Route::get('/attendence/delete/{id}','AttendenceController@destroy');



Route::get('/exam','ExamController@index');
Route::get('/exam/create','ExamController@create');
Route::post('/exam/create','ExamController@store');
Route::get('/exam/edit/{id}','ExamController@edit');
Route::post('/exam/edit/{id}','ExamController@update');
Route::get('/exam/delete/{id}','ExamController@destroy');






Route::get('/mark','MarksController@index');
Route::get('/mark/create','MarksController@create');
Route::post('/mark/create','MarksController@store');
Route::get('/mark/edit/{id}','MarksController@edit');
Route::post('/mark/edit/{id}','MarksController@update');
Route::get('/mark/delete/{id}','MarksController@destroy');



Route::get('/teacher-attendence','TeacherAttendanceController@index');
Route::get('/teacher-attendence/create','TeacherAttendanceController@create');
Route::post('/teacher-attendence/create','TeacherAttendanceController@store');
Route::get('/teacher-attendence/edit/{id}','TeacherAttendanceController@edit');
Route::post('/teacher-attendence/edit/{id}','TeacherAttendanceController@update');
Route::get('/teacher-attendence/delete/{id}','TeacherAttendanceController@destroy');



Route::get('/staff-attendence','StaffAttendenceController@index');
Route::get('/staff-attendence/create','StaffAttendenceController@create');
Route::post('/staff-attendence/create','StaffAttendenceController@store');
Route::get('/staff-attendence/edit/{id}','StaffAttendenceController@edit');
Route::post('/staff-attendence/edit/{id}','StaffAttendenceController@update');
Route::get('/staff-attendence/delete/{id}','StaffAttendenceController@destroy');


Route::get('/result','ResultController@index');
Route::get('/result/create','ResultController@create');
Route::post('/result/create','ResultController@store');
Route::get('/result/edit/{id}','ResultController@edit');
Route::post('/result/edit/{id}','ResultController@update');
Route::get('/result/delete/{id}','ResultController@destroy');