<?php

namespace App\Http\Controllers;

use App\Result;
use App\allClass;
use App\exam;
use App\Student;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Result::all();
        $allclasses = allClass::all();
        $students = Student::all();
        $exams = exam::all();
        return view ('dashboard.result.index',compact('results','allclasses','students','exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allclasses = allClass::all();
        $students = Student::all();
        $exams = exam::all();
        return view ('dashboard.result.create',compact('allclasses','students','exams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $results = new Result;
        $results->class_id = $request->class_id;
        $results->student_id = $request->student_id;
        $results->exam_id = $request->exam_id;
        $results->grade = $request->grade;
        $results->gpa = $request->gpa;
        $results->save();
        return redirect('/result');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $result,$id)
    {
        $results = $result::findOrFail($id);
        $allclasses = allClass::all();
        $students = Student::all();
        $exams = exam::all();
        return view ('dashboard.result.edit',compact('allclasses','students','exams','results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $result,$id)
    {
        $results = $result::findOrFail($id);
        $results->class_id = $request->class_id;
        $results->student_id = $request->student_id;
        $results->exam_id = $request->exam_id;
        $results->grade = $request->grade;
        $results->gpa = $request->gpa;
        $results->save();
        return redirect('/result');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result,$id)
    {
        $results = $result::findOrFail($id)->delete();
        return redirect('/result');
    }
}
