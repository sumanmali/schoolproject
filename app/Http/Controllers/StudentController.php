<?php

namespace App\Http\Controllers;

use App\Student;
use App\Student_class;
Use \DB;
use App\gender;
use App\religion;
use App\Bloodgroup;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return  view('dashboard.student.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genders = gender::all();
        $religions = religion::all();
         $Blood_Group = Bloodgroup::all();
       return  view('dashboard.student.create',compact('Blood_Group','genders','religions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $students = new Student();
        $students->Student_name = $request['name']; 
        $students->DOB = $request['dob']; 
        $students->gender = $request['gender']; 
        $students->Blood_Group = $request['blood_group']; 
        $students->Nationality = $request['nationality'];
        $students->Email = $request['email']; 
        $students->religion = $request['religion']; 
        $students->phone_Number = $request['phone_no']; 
        $students->address = $request['address']; 
        $students->father_name = $request['fname']; 
        $students->mother_name = $request['mname']; 
        $students->father_phn = $request['fphone_no']; 
        $students->mother_phn = $request['mphone_no'];

        if(file_exists($request->photo)){
            $filename = 'img'.time().'.'.$request->photo->getClientOriginalExtension();
            $location = public_path('images/');
            $request->photo->move($location, $filename);
            $students->photo = $filename;
        }
        else{
            $students->photo = 'https://static.thenounproject.com/png/17241-200.png';
        }
        return redirect('/student');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student,$id)
    {
        $students = Student::findorfail($id);
        $genders = gender::all();
        $religions = religion::all();
         $Blood_Group = Bloodgroup::all();
        return view("dashboard.student.edit",compact('students','genders','Blood_Group','religions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student,$id)
    {
        $students = Student::findorfail($id);
        $students->Student_name = $request['name']; 
        $students->DOB = $request['dob']; 
        $students->gender = $request['gender']; 
        $students->Blood_Group = $request['blood_group']; 
        $students->Nationality = $request['nationality'];
        $students->Email = $request['email']; 
        $students->religion = $request['religion']; 
        $students->phone_Number = $request['phone_no']; 
        $students->address = $request['address']; 
        $students->father_name = $request['fname']; 
        $students->mother_name = $request['mname']; 
        $students->father_phn = $request['fphone_no']; 
        $students->mother_phn = $request['mphone_no'];

        if(file_exists($request->photo)){
            $filename = 'img'.time().'.'.$request->photo->getClientOriginalExtension();
            $location = public_path('images/');
            $request->photo->move($location, $filename);
            $students->photo = $filename;
        }
        else{
            $students->photo = $students->photo;
        }
       
         $students->save();

        return redirect('/student');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student,$id)
    {
        $students = Student::findOrFail($id) ->delete();
        return redirect()->back();
   }
}
