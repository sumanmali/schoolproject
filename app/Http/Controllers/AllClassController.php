<?php

namespace App\Http\Controllers;

use App\room;
use App\Group;
use App\allClass;
use Illuminate\Http\Request;

class AllClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group = Group::all();
        $rooms = Room::all();
        $allclasses = allClass::all();
        return view('dashboard.allclass.index',compact('rooms','group','allclasses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = Group::all();
        $rooms = Room::all();
        return view('dashboard.allclass.create',compact('rooms','group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allClasses = new allClass;
        $allClasses->name = $request->name;
        $allClasses->room_id = $request->room_id;
        $allClasses->group_id = $request->group_id;
        $allClasses->save();
        return redirect ('/class');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\allClass  $allClass
     * @return \Illuminate\Http\Response
     */
    public function show(allClass $allClass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\allClass  $allClass
     * @return \Illuminate\Http\Response
     */
    public function edit(allClass $allClass,$id)
    {
        $group = Group::all();
        $rooms = Room::all();
        $allClasses = allClass::findOrFail($id);
        return view ('dashboard.allclass.edit',compact('allClasses','group','rooms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\allClass  $allClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, allClass $allClass,$id)
    {
        $allClasses = allClass::findOrFail($id);
        $allClasses->name = $request->name;
        $allClasses->room_id = $request->room_id;
        $allClasses->group_id = $request->group_id;
        $allClasses->save();
        return redirect('/class');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\allClass  $allClass
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allClasses =  allClass::findOrFail($id) ->delete();
        return back();
    }
}
