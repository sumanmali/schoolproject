<?php

namespace App\Http\Controllers;

use App\staff;
use App\gender;
use App\BloodGroup;

use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = staff::all();
        return  view('dashboard.staff.index',compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return  view('dashboard.staff.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $staffs = new staff();
        $staffs->name = $request['name']; 
       
        $staffs->Blood_Group = $request['blood_group']; 
       
       
        $staffs->designation = $request['designation']; 
        $staffs->number = $request['phone_no']; 
        $staffs->address = $request['address']; 
      

         if(file_exists($request->document)){
            $filename = 'doc'.time().'.'.$request->document->getClientOriginalExtension();
            $location = public_path('images/');
            $request->document->move($location, $filename);
            $staffs->document = $filename;
        }
           else{
            $staffs->document = 'https://static.thenounproject.com/png/17241-200.png';
        }   
         

        if(file_exists($request->photo)){
            $filename = 'img'.time().'.'.$request->photo->getClientOriginalExtension();
            $location = public_path('images/');
            $request->photo->move($location, $filename);
            $staffs->photo = $filename;
        }
        else{
            $staffs->photo = 'https://static.thenounproject.com/png/17241-200.png';
        }
       
         $staffs->save();


        return redirect('/staff');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show(staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(staff $staff ,$id)
    {
       $staffs = staff::find($id);
       $Blood_Group = Bloodgroup::all();
        return view("dashboard.staff.edit",compact('staffs','Blood_Group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, staff $staff ,$id)
    {
        $staffs = staff::findorfail($id);
        $staffs->name = $request['name']; 
       
        $staffs->Blood_Group = $request['blood_group']; 
       
       
        $staffs->designation = $request['designation']; 
        $staffs->number = $request['phone_no']; 
        $staffs->address = $request['address']; 
      

         if(file_exists($request->document)){
            $filename = 'doc'.time().'.'.$request->document->getClientOriginalExtension();
            $location = public_path('images/');
            $request->document->move($location, $filename);
            $staffs->document = $filename;
        }
           else{
            $staffs->document = 'https://static.thenounproject.com/png/17241-200.png';
        }   
         

        if(file_exists($request->photo)){
            $filename = 'img'.time().'.'.$request->photo->getClientOriginalExtension();
            $location = public_path('images/');
            $request->photo->move($location, $filename);
            $staffs->photo = $filename;
        }
        else{
            $staffs->photo = $staffs->photo;
        }
       
         $staffs->save();


        return redirect('/staff');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy(staff $staff ,$id)
    {
       $staffs = staff::find($id)->delete();

       return redirect('/staff');
    }
}
