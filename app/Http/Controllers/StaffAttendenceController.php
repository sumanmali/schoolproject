<?php

namespace App\Http\Controllers;

use App\staffAttendence;
use App\staff;
use Illuminate\Http\Request;

class StaffAttendenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff_attendances = staffAttendence::all();
        $staffs = staff::all();
        return  view('dashboard.staff-attendence.index', compact('staff_attendances','staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staffs = staff::all();
        return view ('dashboard.staff-attendence.create',compact('staffs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $staff_attendances = new staffAttendence();
        $staff_attendances->staff_id = $request->staff_id;
        $staff_attendances->status = $request->status;
        $staff_attendances->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\staffAttendence  $staffAttendence
     * @return \Illuminate\Http\Response
     */
    public function show(staffAttendence $staffAttendence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\staffAttendence  $staffAttendence
     * @return \Illuminate\Http\Response
     */
    public function edit(staffAttendence $staffAttendence ,$id)
    {
        $staff_attendances = staffAttendence::findOrFail($id);
        $staffs = staff::all();
        return view ('dashboard.staff-attendence.edit',compact('staff_attendances','staffs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\staffAttendence  $staffAttendence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, staffAttendence $staffAttendence ,$id)
    {
        $staff_attendances = staffAttendence::findOrFail($id);
        $staff_attendances->staff_id = $request->staff_id;
        $staff_attendances->status = $request->status;
        $staff_attendances->save();
        return redirect('/staff-attendence');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\staffAttendence  $staffAttendence
     * @return \Illuminate\Http\Response
     */
    public function destroy(staffAttendence $staffAttendence ,$id)
    {
       $staff_attendances = staffAttendence::findOrFail($id)->delete();
        return redirect('/staff-attendence');
    }
}
