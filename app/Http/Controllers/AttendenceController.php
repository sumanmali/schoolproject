<?php

namespace App\Http\Controllers;

use App\attendence;
use App\Student;
use App\allClass;
use App\Section;
use Illuminate\Http\Request;

class AttendenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $attendence = attendence::all();
      $students = Student::all();
      $sections = Section::all();
      $allclasses = allClass::all();
         return  view('dashboard.attendence.index', compact('attendence','students','sections', 'allclasses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        $allclasses = allClass::all();
        $sections = Section::all();
       return  view('dashboard.attendence.create' , compact('allclasses','students','sections')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attendence = new attendence();
        $attendence->class_id = $request->class_id;
        $attendence->student_id = $request->student_id;
        $attendence->status = $request->status;
        $attendence->section_id = $request->sections;
         $attendence->save();
        return redirect('/attendence');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\attendence  $attendence
     * @return \Illuminate\Http\Response
     */
    public function show(attendence $attendence)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\attendence  $attendence
     * @return \Illuminate\Http\Response
     */
    public function edit(attendence $attendence ,$id)
    {
      $students = Student::all();
        $allclasses = allClass::all();
        $sections = Section::all();
        $attendence = attendence::findOrFail($id);
       return  view('dashboard.attendence.edit' , compact('allclasses','students','sections', 'attendence')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\attendence  $attendence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, attendence $attendence ,$id)
    {
       $attendence = attendence::findOrfail($id);

       $attendence->class_id = $request->class_id;
        $attendence->student_id = $request->student_id;
        $attendence->status = $request->status;
        $attendence->section_id = $request->sections;
         $attendence->save();

        return redirect('/attendence');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\attendence  $attendence
     * @return \Illuminate\Http\Response
     */
    public function destroy(attendence $attendence,$id)
    {
       
        $attendence = attendence::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
