<?php

namespace App\Http\Controllers;

use App\teacher_attendance;
use App\teacher;
use Illuminate\Http\Request;

class TeacherAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacher_attendances = teacher_attendance::all();
        $teachers = teacher::all();
        return  view('dashboard.teacher-attendence.index', compact('teacher_attendances','teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = teacher::all();
        return view ('dashboard.teacher-attendence.create',compact('teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacher_attendances = new teacher_attendance();
        $teacher_attendances->teacher_id = $request->teacher_id;
        $teacher_attendances->status = $request->status;
        $teacher_attendances->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\teacher_attendance  $teacher_attendance
     * @return \Illuminate\Http\Response
     */
    public function show(teacher_attendance $teacher_attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\teacher_attendance  $teacher_attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(teacher_attendance $teacher_attendance,$id)
    {
        
        $teacher_attendances = teacher_attendance::findOrFail($id);
        $teachers = teacher::all();
        return view ('dashboard.teacher-attendence.edit',compact('teacher_attendances','teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\teacher_attendance  $teacher_attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, teacher_attendance $teacher_attendance,$id)
    {
        $teacher_attendances = teacher_attendance::findOrFail($id);
        $teacher_attendances->teacher_id = $request->teacher_id;
        $teacher_attendances->status = $request->status;
        $teacher_attendances->save();
        return redirect('/teacher-attendence');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\teacher_attendance  $teacher_attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(teacher_attendance $teacher_attendance,$id)
    {
        $teacher_attendances = teacher_attendance::findOrFail($id)->delete();
        return redirect('/teacher-attendence');
    }
}
