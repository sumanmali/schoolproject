<?php

namespace App\Http\Controllers;

use App\marks;
use App\Student;
use App\allClass;
use App\Section;
use App\exam;
use App\subject;
use Illuminate\Http\Request;

class MarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $students = Student::all();
        $allclasses = allClass::all();
        $sections = Section::all();
        $exams = exam::all();
        $subjects = subject::all();
        $allmarks = marks::all();
     return  view('dashboard.mark.index' , compact('allclasses','students','sections','exams','subjects','allmarks')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $students = Student::all();
        $allclasses = allClass::all();
        $sections = Section::all();
        $exams = exam::all();
        $subjects = subject::all();
        return  view('dashboard.mark.create' , compact('allclasses','students','sections','exams','subjects')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $mark = new marks();
      $mark->class_id = $request->class_id;
        $mark->student_id = $request->student_id;
        $mark->section_id = $request->sections;
        $mark->subject_id = $request->subject_id;
        $mark->exam_id = $request->exam;
        $mark->tmarks = $request->tmarks;
        $mark->pmarks = $request->pmarks;
        $mark->total_marks = $request->totalmarks;

         $mark->save();
        return redirect('/mark');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\marks  $marks
     * @return \Illuminate\Http\Response
     */
    public function show(marks $marks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\marks  $marks
     * @return \Illuminate\Http\Response
     */
    public function edit(marks $marks, $id)
    {
        
       $students = Student::all();
        $allclasses = allClass::all();
        $sections = Section::all();
        $exams = exam::all();
        $subjects = subject::all();
        $marks = marks::findOrfail($id);
        return  view('dashboard.mark.edit' , compact('allclasses','students','sections','exams','subjects','marks')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\marks  $marks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, marks $marks ,$id)
    {
         $mark = marks::findOrfail($id);

       $mark->class_id = $request->class_id;
        $mark->student_id = $request->student_id;
        $mark->section_id = $request->sections;
        $mark->subject_id = $request->subject_id;
        $mark->exam_id = $request->exam;
        $mark->tmarks = $request->tmarks;
        $mark->pmarks = $request->pmarks;
        $mark->total_marks = $request->totalmarks;

         $mark->save();
        return redirect('/mark');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\marks  $marks
     * @return \Illuminate\Http\Response
     */
    public function destroy(marks $marks ,$id)
    {
        
        $marks = marks::findOrFail($id) ->delete();
        return redirect('/mark');
    }
}
