<?php

namespace App\Http\Controllers;

use App\guardian;
use Illuminate\Http\Request;

class GuardianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guardians = guardian::all();
        return view('dashboard.guardian.index',compact('guardians'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.guardian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $guardians = new guardian;
        $guardians->name = $request->name;
        $guardians->phone_no = $request->phone_no;
        $guardians->mobile_no = $request->mobile_no;
        $guardians->address = $request->address;
        $guardians->email = $request->email;
        if(file_exists($request->file('image'))){
            $image = "guardian".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $guardians->image = $image;
        }
        else{
            $guardians->image = 'default-thumbnail.png';
        }
        $guardians->save();
        return redirect('/guardian');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\guardian  $guardian
     * @return \Illuminate\Http\Response
     */
    public function show(guardian $guardian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\guardian  $guardian
     * @return \Illuminate\Http\Response
     */
    public function edit(guardian $guardian,$id)
    {
         $guardians = guardian::findOrFail($id);
         return view ('dashboard.guardian.edit',compact('guardians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\guardian  $guardian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, guardian $guardian,$id)
    {
        $guardians = guardian::findOrFail($id);
        $guardians->name = $request->name;
        $guardians->phone_no = $request->phone_no;
        $guardians->mobile_no = $request->mobile_no;
        $guardians->address = $request->address;
        $guardians->email = $request->email;
        if(file_exists($request->file('image'))){
            $image = "guardian".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $guardians->image = $image;
        }
        else{
            $guardians->image = $guardians->image;
        }
        $guardians->save();
        return redirect('/guardian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\guardian  $guardian
     * @return \Illuminate\Http\Response
     */
    public function destroy(guardian $guardian,$id)
    {
         $guardians = guardian::findOrFail($id)->delete();
    }
}
