<?php

namespace App\Http\Controllers;

use App\room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms=Room::all();
        return view ('dashboard.room.index',compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.room.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rooms = New Room;
        $rooms->room_no = $request->room_no;
        $rooms->capacity = $request->capacity;
        $rooms->floor = $request->floor;
        $rooms->block = $request->block;
        $rooms->save();
        return redirect('/room');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(room $room,$id)
    {
        $rooms = Room::findOrFail($id);
        return view ('dashboard.room.edit',compact('rooms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, room $room,$id)
    {
        $rooms = Room::findOrFail($id);
        $rooms->room_no = $request->room_no;
        $rooms->capacity = $request->capacity;
        $rooms->floor = $request->floor;
        $rooms->block = $request->block;
        $rooms->save();
        return redirect('/room');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rooms = Room::findOrFail($id) ->delete();
        return back();
    }
}
