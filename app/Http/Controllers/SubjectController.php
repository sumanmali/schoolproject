<?php

namespace App\Http\Controllers;

use App\subjectType;
use App\subject;
use App\allClass;
use App\teacher;
use App\attendence;

use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allclasses = allClass::all();
        $teachers = teacher::all();
        $subjects = subject::all();
        return view('dashboard.subject.index',compact('subjects','teachers','allclasses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allclasses = allClass::all();
        $teachers = teacher::all();
        return view('dashboard.subject.create',compact('allclasses','teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subjects = new subject();
        $subjects->name = $request->name;
        $subjects->code = $request->code;
        $subjects->type_id = $request->type_id;
        $subjects->class_id = $request->class_id;
        $subjects->teacher_id = $request->teacher_id;
        $subjects->save();
        return redirect('/subjects');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(subject $subject,$id)
    {
        $allclasses = allClass::all();
        $teachers = teacher::all();
        $subjects = subject::findOrFail($id);
        return view('dashboard.subject.edit',compact('allclasses','teachers','subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subject $subject,$id)
    {
        $subjects = subject::findOrFail($id);
        $subjects->name = $request->name;
        $subjects->code = $request->code;
        $subjects->type_id = $request->type_id;
        $subjects->class_id = $request->class_id;
        $subjects->teacher_id = $request->teacher_id;
        $subjects->save();
        return redirect('/subjects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subjects = subject::findOrFail($id)->delete();
        return redirect()->back();
    }
}
