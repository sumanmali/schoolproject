<?php

namespace App\Http\Controllers;

use App\exam;
use App\allClass;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allclasses = allClass::all();
        $exams =  exam::all();
        return  view('dashboard.exam.index', compact('allclasses' ,'exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allclasses = allClass::all();
           return  view('dashboard.exam.create' , compact('allclasses')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $exams = new exam();
        $exams->name = $request->name;
        $exams->class_id = $request->class_id;
        $exams->acedemic_year = $request->acedemic_year;

        $exams->save();

        return redirect('/exam');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(exam $exam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(exam $exam ,$id)
    {

        $allclasses = allClass::all();
         $exams =  exam::findOrfail($id);
        return  view('dashboard.exam.edit' , compact('allclasses','exams')); 
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, exam $exam ,$id)
    {
       $exams =  exam::findOrfail($id); 
        $exams->name = $request->name;
        $exams->class_id = $request->class_id;
        $exams->acedemic_year = $request->acedemic_year;

        $exams->save();

        return redirect('/exam');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(exam $exam ,$id)
    {
         $exams = exam::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
