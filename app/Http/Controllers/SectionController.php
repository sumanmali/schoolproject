<?php

namespace App\Http\Controllers;

use App\teacher;
use App\allClass;
use App\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Section::all();
        $teachers = Teacher::all();
        $allClasses = allClass::all();
        return view('dashboard.section.index',compact('teachers','allClasses','sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = Teacher::all();
        $allClasses = allClass::all();
        return view ('dashboard.section.create',compact('teachers','allClasses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sections = new Section();
        $sections->name = $request->name;
        $sections->class_teacher_id = $request->class_teacher_id;
        $sections->class_id = $request->class_id;
        $sections->save();
        return redirect('/section');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section,$id)
    {
        $teachers = Teacher::all();
        $allClasses = allClass::all();
        $sections = Section::findOrFail($id);
        return view('dashboard.section.edit',compact('teachers','allClasses','sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section,$id)
    {
        $sections = Section::findOrFail($id);
        $sections->name = $request->name;
        $sections->class_teacher_id = $request->class_teacher_id;
        $sections->class_id = $request->class_id;
        $sections->save();
        return redirect('/section');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section,$id)
    {
         $sections = Section::findOrFail($id)->delete();
         return back();
    }
}
