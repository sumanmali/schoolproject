<?php

namespace App\Http\Controllers;

use App\teacher;
use App\Gender;
use App\Bloodgroup;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::all();
        return view ('dashboard.teacher.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genders = Gender::all();
        $bloodgroups = Bloodgroup::all();
        return view ('dashboard/teacher/create', compact('genders','bloodgroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teachers = new teacher();
        $teachers->name = $request->name;
        $teachers->designation = $request->designation;
        $teachers->qualification = $request->qualification;
        $teachers->dob = $request->dob;
        $teachers->gender = $request->gender;
        $teachers->email = $request->email;
        $teachers->phone = $request->phone;
        $teachers->joindate = $request->joindate;
        $teachers->address = $request->address;
        $teachers->nationality = $request->nationality;
        $teachers->bloodgroup = $request->bloodgroup;
         if(file_exists($request->file('photo'))){
            $photo = "teacher".time().'.'.$request->file('photo')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('photo')->move($location, $photo);
            $teachers->photo = $photo;
        }
        else{
            $teachers->photo = 'default-thumbnail.png';
        }
        $teachers->save();
        return redirect ('/teacher');  

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(teacher $teacher,$id)
    {
        $bloodgroups = Bloodgroup::all();
        $teachers = Teacher::findOrFail($id);
        return view ('dashboard/teacher/edit', compact('teachers','bloodgroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, teacher $teacher,$id)
    {
        $teachers = Teacher::findOrFail($id);
        $teachers->name = $request->name;
        $teachers->designation = $request->designation;
        $teachers->qualification = $request->qualification;
        $teachers->dob = $request->dob;
        $teachers->gender = $request->gender;
        $teachers->email = $request->email;
        $teachers->phone = $request->phone;
        $teachers->joindate = $request->joindate;
        $teachers->address = $request->address;
        $teachers->nationality = $request->nationality;
        $teachers->bloodgroup = $request->bloodgroup;
         if(file_exists($request->file('photo'))){
            $photo = "teacher".time().'.'.$request->file('photo')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('photo')->move($location, $photo);
            $teachers->photo = $photo;
        }
        else{
            $teachers->photo = $teachers->photo;
        }
        $teachers->save();
        return redirect ('/teacher');  

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teachers = Teacher::findOrFail($id) ->delete();
        return redirect('/teacher');
    }
}
