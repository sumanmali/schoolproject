@extends('adminlayout.app')

@section('content')
<div class='col-sm-9'>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> Staff Attendence</h1>
                </div>
            </div>
        </div>
    </div>
    <section class='content'>
        <div class='container-fluid'>
            <p>
                <a href="/staff-attendence/create" class="btn btn-primary">Add new Record</a>
            </p>
            <div class='row'>
                <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Staff Name</th>
                        <th>Status</th>
                        <th>Action</th>

                    </thead>
             
                @foreach($staff_attendances as $attendence)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    @foreach($staffs as $staff)
                   @if($attendence->staff_id === $staff->id)
                    <td>{{$staff->name}}</td>
                     @endif
                     @endforeach

                     @if($attendence->status == 0)
                    <td>absent</td>
                    @else
                    <td>present</td>
                    @endif

                   
                   
            
                    <td><a href="/staff-attendence/edit/{{$attendence->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a  onclick="makeWarning(event)"  href="/staff-attendence/delete/{{$attendence->id}}" class="btn btn-danger">Delete</a></td>

                </tr>
               
                @endforeach
            </table>
        </div>


        </div>
    </section>
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
            if(! result){
                evt.stopPropagation();
                evt.preventDefault();   
            }
    }
@endsection
