@extends('adminlayout.app')

@section('content')

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/staff/edit/{{$staffs->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Staff Info:</p>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="name" value="{{$staffs->name}}" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="designation">Designation</label>
                                        <input  type="designation" class="form-control" name="designation" value="{{$staffs->designation}}" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="blood_group">Blood Group<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select blood group type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="blood_group" value="{{$staffs->Blood_Group}}">
                                            @foreach($Blood_Group as $Blood)
                                            @if($Blood->id == $staffs->Blood)
                                                <option value="{{$Blood->id}}">{{$Blood->name}}</option>
                                            @endif
                                        @endforeach 
                                        @foreach($Blood_Group as $Blood)
                                            @if($Blood->id != $staffs->Blood)
                                            
                                                <option value="{{$Blood->id}}">{{$Blood->name}}</option>
                                            @endif
                                        @endforeach   
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                                     

                            </div>
                           
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="photo">Photo<span class="text-danger"></span></label>
                                        <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photo" placeholder="Photo image">
                                        <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="document">Document<span class="text-danger"></span></label>
                                        <input  type="file" class="form-control" accept=".doc, .txt, .pdf" name="document" placeholder="document file">
                                        <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="address">Address</label>
                                        <input  type="address" class="form-control" name="address"  value="{{$staffs->address}}" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="phone_no">Phone/Mobile No.</label>
                                        <input  type="text" class="form-control" name="phone_no"  value="{{$staffs->number}}" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                            </div> 
                           
                        <!-- /.box-body -->
                        <div class="box-footer">
                           
                          <button type='submit' class='button is-link'>Update</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection