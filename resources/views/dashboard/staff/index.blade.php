@extends('adminlayout.app')

@section('content')
<div class='col-sm-9'>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Staff</h1>
                </div>
            </div>
        </div>
    </div>
    <section class='content'>
        <div class='container-fluid'>
            <p>
                <a href="/staff/create" class="btn btn-primary">Add new staff</a>
            </p>
            <div class='row'>
                <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Photo</th>
                        <th>Phone No.</th>
                        <th>Document</th>
                        
                        <th>Address</th>
                        <th>Action</th>

                    </thead>
             
                @foreach($staffs as $staff)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{$staff->name}}</td>
                    <td><img src="{{ asset('/images/'.$staff->photo )}}" style="width:300px;"></td>
                    <td>{{$staff->number}}</td>
                     <td><img src="{{ asset('/images/'.$staff->document )}}" style="width:300px;"></td>
                    
                    <td>{{$staff->address}}</td>
                    <td><a href="/staff/edit/{{$staff->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a onclick="makeWarning(event)" href="/staff/delete/{{$staff->id}}" class="btn btn-danger">Delete</a></td>

                </tr>
               
                @endforeach
            </table>
        </div>


        </div>
    </section>
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
            if(! result){
                evt.stopPropagation();
                evt.preventDefault();   
            }
    }
@endsection
