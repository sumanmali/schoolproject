@extends('adminlayout.app')

@section('content')

<div class="col-sm-9">

	<section class="content-header">
		<h1>
			Guardian
			<small>-> update </small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="/guardian"><i class="fa icon-guardian"></i>-> Guardian</a></li>
			<li class="active">-> update </li>
		</ol>
	</section>
	<div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<form novalidate id="entryForm" action="/guardian/edit/{{$guardians->id}}" method="post" enctype="multipart/form-data">
						@csrf
						<div class="box-body">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="name">Guardain Name<span class="text-danger">*</span></label>
										<input autofocus type="text" class="form-control" name="name" value="{{$guardians->name}}" required minlength="2" maxlength="10">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="address">address</label>
										<input type="text" class="form-control" name="address" value="{{$guardians->address}}"  maxlength="55">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="email">email</label>
										<input type="email" class="form-control" name="email" value="{{$guardians->email}}"  maxlength="55">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="phone_no">phone Number</label>
										<input type="text" class="form-control" name="phone_no" value="{{$guardians->phone_no}}"  maxlength="55">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="mobile_no">Mobile Number</label>
										<input type="text" class="form-control" name="mobile_no" value="{{$guardians->mobile_no}}"  maxlength="55">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="image">Photo<br><span class="text-danger">[min 150 X 150 size and max 200kb]</span></label>
										<img style="max-width: 50%;" src="/uploads/{{$guardians->image}}" alt="{{$guardians->name}}">
										<input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="image" placeholder="Photo image">
										<span class="glyphicon glyphicon-open-file form-control-feedback" style="top:45px;"></span>
										<span class="text-danger"></span>
									</div>
								</div>							
							</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> update
							</button>
							<button type="reset" class="btn btn-danger btn-sm">
								<i class="fa fa-ban"></i> Reset
							</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<!-- =======
<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/parent/edit/{{$guardians->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Guardian Info:</p>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="name" value="{{$guardians->name}}" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="email">Email</label>
                                        <input  type="email" class="form-control" name="email"  placeholder="email address" value="{{$guardians->email}}" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="blood_group">Blood Group<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select blood group type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="blood_group" value="{{$guardians->Blood_Group}}">
                                            <option value="0"selected="selected" >------------</option>
                                            <option value="1" >A+</option>
                                            <option value="2">O+</option>
                                            <option value="3">B+</option>
                                            <option value="4">AB+</option>
                                            <option value="5">A-</option>
                                            <option value="6">O-</option>
                                            <option value="7">B-</option>
                                            <option value="8">AB-</option>
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                                     

                            </div>
                           
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="photo">Photo<span class="text-danger"></span></label>
                                        <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photo"  value="{{$guardians->photo}}">
                                        <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="address">Address</label>
                                        <input  type="email" class="form-control" name="address"  placeholder=" address" value="{{$guardians->address}}" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="phone_no">Phone/Mobile No.</label>
                                        <input  type="text" class="form-control" name="phone_no" placeholder="phone or mobile number" value="{{$guardians->number}}" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                            </div> 
                           
             
                        <div class="box-footer">
                            <button type='submit' class='button is-link'>Update</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
>>>>>>> e35a3e12119a1508bb314f2d7afe1dae7dc67d2c -->
</div>
@endsection