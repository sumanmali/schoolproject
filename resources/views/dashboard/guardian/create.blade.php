@extends('adminlayout.app')

@section('content')

<div class="col-sm-9">

	<section class="content-header">
		<h1>
			Guardian
			<small>-> Add New </small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="/guardian"><i class="fa icon-guardian"></i>-> Guardian</a></li>
			<li class="active">-> Add </li>
		</ol>
	</section>
	<div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<form novalidate id="entryForm" action="/guardian/create" method="post" enctype="multipart/form-data">
						@csrf
						<div class="box-body">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="name">Guardain Name<span class="text-danger">*</span></label>
										<input autofocus type="text" class="form-control" name="name" placeholder="Guardian Name" required minlength="2" maxlength="10">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="address">Address</label>
										<input type="text" class="form-control" name="address" placeholder="Guardain Address" value=""  maxlength="55">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="email">Email</label>
										<input type="email" class="form-control" name="email" placeholder="Guardain Email" value=""  maxlength="55">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="phone_no">Phone Number</label>
										<input type="text" class="form-control" name="phone_no" placeholder="Guardain Phone Number">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="mobile_no">Mobile Number</label>
										<input type="text" class="form-control" name="mobile_no" placeholder="Guardain Mobile Number">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="image">Photo<br><span class="text-danger">[min 150 X 150 size and max 200kb]</span></label>
										<input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="image" placeholder="Photo image">
										<span class="glyphicon glyphicon-open-file form-control-feedback" style="top:45px;"></span>
										<span class="text-danger"></span>
									</div>
								</div>							
							</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Submit
							</button>
							<button type="reset" class="btn btn-danger btn-sm">
								<i class="fa fa-ban"></i> Reset
							</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<!-- <<<<<<< HEAD
=======

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/parent/create" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Guardian Info:</p>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="name" value="" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="email">Email</label>
                                        <input  type="email" class="form-control" name="email"  placeholder="email address" value="" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="blood_group">Blood Group<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select blood group type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="blood_group">
                                            <option value="0"selected="selected" >------------</option>
                                            <option value="1" >A+</option>
                                            <option value="2">O+</option>
                                            <option value="3">B+</option>
                                            <option value="4">AB+</option>
                                            <option value="5">A-</option>
                                            <option value="6">O-</option>
                                            <option value="7">B-</option>
                                            <option value="8">AB-</option>
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                                     

                            </div>
                           
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="photo">Photo<span class="text-danger"></span></label>
                                        <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photo" placeholder="Photo image">
                                        <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="address">Address</label>
                                        <input  type="email" class="form-control" name="address"  placeholder=" address" value="" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="phone_no">Phone/Mobile No.</label>
                                        <input  type="text" class="form-control" name="phone_no" placeholder="phone or mobile number" value="" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                            </div> 
                           
                        <div class="box-footer">
                            <a href="/student" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-plus-circle "></i>Add </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

>>>>>>> 27d854437795438325360f3dccf566fc4f9584bc -->
</div>
@endsection