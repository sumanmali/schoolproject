@extends('adminlayout.app')

@section('content')
<div class='col-sm-9'>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">All Result</h1>
                </div>
            </div>
        </div>
    </div>
    <section class='content'>
        <div class='container-fluid'>
            <p>
                <a href="/result/create" class="btn btn-primary">Add new Result</a>
            </p>
            <div class='row'>
                <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Class</th>
                        <th>Student</th>
                        <th>Grade</th>
                        <th>GPA</th>
                        <th>Exam</th>
                        <th>Action</th>

                    </thead>
             
                @foreach($results as $result)
                <tr>
                    <td>{{ $loop->iteration }}</td>

                    @foreach($allclasses as $class)
                   @if($result->class_id === $class->id)
                    <td>{{$class->name}}</td>
                     @endif
                     @endforeach

                     @foreach($students as $student)
                    @if($result->student_id == $student->id)
                     <td>{{$student->Student_name}}</td>
                    @endif
                    @endforeach

                <td>{{$result->grade}}</td>

                <td>{{$result->gpa}}</td>


                    @foreach($exams as $exam)
                   @if($result->exam_id === $exam->id)
                    <td>{{$exam->name}}</td>
                     @endif
                     @endforeach
            
                    <td><a href="#" class="btn btn-info" data-toggle="modal" data-target="#myModal">View</a>   &nbsp; <a href="/result/edit/{{$result->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a onclick="makeWarning(event)" href="/result/delete/{{$result->id}}" class="btn btn-danger">Delete</a></td>

                </tr>
               
                @endforeach
            </table>
        </div>


        </div>
    </section>
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
            if(! result){
                evt.stopPropagation();
                evt.preventDefault();   
            }
    }
</script>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <h3>Marksheet</h3>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Class</th>
                        <th>Student</th>
                        <th>Grade</th>
                        <th>GPA</th>
                        <th>Exam</th>

                    </thead>
             
                @foreach($results as $results)
                <tr>
                    <td>{{ $loop->iteration }}</td>

                    @foreach($allclasses as $class)
                   @if($results->class_id === $class->id)
                    <td>{{$class->name}}</td>
                     @endif
                     @endforeach

                     @foreach($students as $student)
                    @if($results->student_id == $student->id)
                     <td>{{$student->Student_name}}</td>
                    @endif
                    @endforeach

                <td>{{$results->grade}}</td>

                <td>{{$results->gpa}}</td>


                    @foreach($exams as $exam)
                   @if($results->exam_id === $exam->id)
                    <td>{{$exam->name}}</td>
                     @endif
                     @endforeach

                </tr>
               
                @endforeach
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  @endsection