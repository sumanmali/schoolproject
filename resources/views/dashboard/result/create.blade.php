@extends('adminlayout.app')

@section('content')

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/result/create" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Marks Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose Class<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="class_id">
                                            <option>-----</option>
                                            @foreach($allclasses as $class)
                                            <option value="{{$class->id}}">{{$class->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="student_id">Choose student<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select student_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="student_id">
                                            <option>-----</option>
                                            @foreach($students as $student)
                                            <option value="{{$student->id}}">{{$student->Student_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="exam_id"> Choose Exam<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select exam_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="exam_id">
                                            <option>-----</option>
                                            @foreach($exams as $exam)
                                            <option value ="{{$exam->id}}">{{$exam->name}}-
                                                @foreach($allclasses as $allclass)
                                                @if($exam->class_id == $allclass->id)
                                                {{$allclass->name}}_class
                                                @endif
                                                @endforeach
                                            </option>
                                            @endforeach                                            
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="grade">Grade<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="grade" placeholder="Grade">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                   </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="gpa">Grade Point Average (GPA)<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="gpa" placeholder="GPA">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>


                            </div>


                            

                        </div>
                       <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection