  @extends('adminlayout.app')

  @section('content')
  <div class="col-sm-9">
    <section class="content-header">
        <h1>
            Subject
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> >Subject List</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="/subject/create"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                            <table id="listDataTableWithSearch" class="table table-bordered table-striped list_view_table display responsive no-wrap" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th class="notexport" width="10%">Subject Name</th>
                                        <th width="10%">Subject Code</th>
                                        <th width="25%">Type</th>
                                        <th width="8%">Class</th>
                                        <th class="notexport" width="15%">Teacher</th>
                                        <th class="notexport" width="15%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($subjects as $subject)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{$subject->name}}</td>
                                        <td>{{$subject->code}}</td>

                                        @if($subject->type_id == 0)
                                        <td>Compulsary</td>
                                        @else
                                        <td>Elective</td>
                                        @endif

                                        @foreach($allclasses as $class)
                                       @if($subject->class_id === $class->id)
                                        <td>{{$class->name}}</td>
                                         @endif
                                         @endforeach
                                         @foreach($teachers as $teacher)
                                       @if($subject->teacher_id === $teacher->id)
                                        <td>{{$teacher->name}}</td>
                                         @endif
                                         @endforeach
                                        <td>
                                            <div class="btn-group">
                                                <a title="Edit" href="/subject/edit/{{$subject->id}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                            </a>
                                        </div>
                                        <!-- todo: have problem in mobile device -->
                                        <div class="btn-group">
                                            <form method="post" action="/subject/delete/{{$subject->id}}">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit" onclick="makeWarning(event)" class="btn btn-danger btn-sm" title="Delete">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                            
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
        if(! result){
            evt.stopPropagation();
            evt.preventDefault();   
        }
    }
</script>
@endsection