@extends('adminlayout.app')

@section('content')

<div class="col-sm-9">

	<section class="content-header">
		<h1>
			Subject
			<small>-> Add New </small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="/subjects"><i class="fa icon-room"></i>-> Subject</a></li>
			<li class="active">-> Add </li>
		</ol>
	</section>
	<div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<form novalidate id="entryForm" action="/subject/create" method="post" enctype="multipart/form-data">
						@csrf
						<div class="box-body">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="name">Subject Name<span class="text-danger">*</span></label>
										<input autofocus type="text" class="form-control" name="name" placeholder="Subject Name" required minlength="2" maxlength="10">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="code">Subject Code</label>
										<input type="text" class="form-control" name="code" placeholder="Subject Code" value=""  maxlength="255">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="type_id">Choose Type<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select type_id type"></i>
										</label>
										<select class="form-control select2" required="true" name="type_id">
										<option >-----</option>
										<option value="0">Compulsary</option>
										<option value="1">Elective</option>
											</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="class_id">Choose Class<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
										</label>
										<select class="form-control select2" required="true" name="class_id">
												<option>-----</option>
											@foreach($allclasses as $class)
												<option value="{{$class->id}}">{{$class->name}}</option>
											@endforeach
											</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="teacher_id">Choose Teacher<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select teacher_id type"></i>
										</label>
										<select class="form-control select2" required="true" name="teacher_id">
											<option>-----</option>
											@foreach($teachers as $teacher)
												<option value="{{$teacher->id}}">{{$teacher->name}}</option>
											@endforeach
											</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>								
							</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Submit
							</button>
							<button type="reset" class="btn btn-danger btn-sm">
								<i class="fa fa-ban"></i> Reset
							</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection