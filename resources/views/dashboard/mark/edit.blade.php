@extends('adminlayout.app')

@section('content')

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/mark/edit/{{$marks->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Marks Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose Class<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="class_id">
                                             @foreach($allclasses as $class)
                                            @if($class->id ==  $marks->id)
                                            <option value="{{$class->id}}">{{$class->name}}</option>
                                            @endif
                                            @endforeach

                                             @foreach($allclasses as $class)
                                            @if($class->id !==  $marks->id)
                                            <option value="{{$class->id}}">{{$class->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose student<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="student_id">
                                            @foreach($students as $student)
                                            @if($student->id == $marks->id)
                                            <option value="{{$student->id}}">{{$student->Student_name}}</option>
                                            @endif
                                            @endforeach


                                             @foreach($students as $student)
                                            @if($student->id !== $marks->id)
                                            <option value="{{$student->id}}">{{$student->Student_name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose section_id<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="sections">
                                            @foreach($sections as $section)
                                            @if($section->id == $marks->id )
                                            <option value="{{$section->id}}">{{$section->name}}</option>
                                            @endif
                                            @endforeach

                                             @foreach($sections as $section)
                                            @if($section->id !== $marks->id )
                                            <option value="{{$section->id}}">{{$section->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id"> Choose subject_id<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="subject_id">
                                           
                                            @foreach($subjects as $subject)
                                            @if($subject->id == $marks->id)
                                            <option value ="{{$subject->id}}">{{$subject->name}}</option>
                                            @endif
                                            @endforeach  

                                            @foreach($subjects as $subject)
                                            @if($subject->id !== $marks->id)
                                            <option value ="{{$subject->id}}">{{$subject->name}}</option>
                                            @endif
                                            @endforeach                                            
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose exam<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="exam">
                                           
                                            @foreach($exams as $exam)
                                            @if($exam->id == $marks->id)
                                            <option value="{{$exam->id}}">{{$exam->name}}</option>
                                            @endif
                                            @endforeach

                                            @foreach($exams as $exam)
                                            @if($exam->id !== $marks->id)
                                            <option value="{{$exam->id}}">{{$exam->name}}</option>
                                            @endif
                                            @endforeach

                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="tmarks">Theorymarks<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="tmarks"  value="{{$marks->tmarks}}" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                   </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="pmarks">Practicalmarks<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="pmarks"  value="{{$marks->pmarks}}" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="totalmark">Totalmarks<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="totalmarks"  value="{{$marks->total_marks}}" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>


                            </div>


                            

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="/mark" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-plus-circle "></i> Update </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection