@extends('adminlayout.app')

@section('content')
<div class='col-sm-9'>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">All Students Marks</h1>
                </div>
            </div>
        </div>
    </div>
    <section class='content'>
        <div class='container-fluid'>
            <p>
                <a href="/mark/create" class="btn btn-primary">Add new Marks</a>
            </p>
            <div class='row'>
                <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Student</th>
                        <th>Class</th>
                        <th>Subject</th>
                        <th>Section</th>
                        <th>Theory Marks</th>
                        <th>Practical Marks</th>
                        <th>Total Marks</th>
                        <th>Exam</th>
                        <th>Action</th>

                    </thead>
             
                @foreach($allmarks as $mark)
                <tr>
                    <td>{{ $loop->iteration }}</td>

                     @foreach($students as $student)
                    @if($mark->student_id == $student->id)
                     <td>{{$student->Student_name}}</td>
                    @endif
                    @endforeach
                    
                    @foreach($allclasses as $class)
                   @if($mark->class_id === $class->id)
                    <td>{{$class->name}}</td>
                     @endif
                     @endforeach
                  
                 @foreach($subjects as $subject)
                    @if($mark->subject_id == $subject->id)
                     <td>{{$subject->name}}</td>
                    @endif
                    @endforeach

                  @foreach($sections as $section)
                 @if($mark->section_id == $section->id)
                   <td>{{$section->name}}</td>
                @endif
                @endforeach

                <td>{{$mark->tmarks}}</td>

                <td>{{$mark->pmarks}}</td>

                 <td>{{$mark->total_marks}}</td>

                    @foreach($exams as $exam)
                   @if($mark->exam_id === $exam->id)
                    <td>{{$exam->name}}</td>
                     @endif
                     @endforeach
            
                    <td><a href="/mark/edit/{{$mark->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a onclick="makeWarning(event)" href="/mark/delete/{{$mark->id}}" class="btn btn-danger">Delete</a></td>

                </tr>
               
                @endforeach
            </table>
        </div>


        </div>
    </section>
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
            if(! result){
                evt.stopPropagation();
                evt.preventDefault();   
            }
    }
</script>
@endsection
