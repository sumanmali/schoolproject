@extends('adminlayout.app')

@section('content')

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/teacher-attendence/edit/{{$teacher_attendances->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Teacher Attendence Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="teacher_id">Choose Teacher<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select teacher_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="teacher_id">
                                           @foreach($teachers as $teachers)
                                            @if($teachers->id ==  $teacher_attendances->id)
                                            <option value="{{$teachers->id}}">{{$teachers->name}}</option>
                                            @endif
                                            @endforeach

                                           
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="status">Choose status<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select status type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="status">
                                           @if($teacher_attendances->status)
                                                <option value="1">Present</option>
                                                <option value="0">Absent</option>

                                            @else
                                                <option value="0">Absent</option>
                                                <option value="1">Present</option>

                                           @endif

                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>


                            </div>
                            

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="/student" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection