@extends('adminlayout.app')

@section('content')
<div class='col-sm-9'>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> Teacher Attendence</h1>
                </div>
            </div>
        </div>
    </div>
    <section class='content'>
        <div class='container-fluid'>
            <p>
                <a href="/teacher-attendence/create" class="btn btn-primary">Add new Record</a>
            </p>
            <div class='row'>
                <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Teacher Name</th>
                        <th>Status</th>
                        <th>Action</th>

                    </thead>
             
                @foreach($teacher_attendances as $attendence)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    @foreach($teachers as $teacher)
                   @if($attendence->teacher_id === $teacher->id)
                    <td>{{$teacher->name}}</td>
                     @endif
                     @endforeach

                     @if($attendence->status == 0)
                    <td>absent</td>
                    @else
                    <td>present</td>
                    @endif

                   
                   
            
                    <td><a href="/teacher-attendence/edit/{{$attendence->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a  onclick="makeWarning(event)" href="/teacher-attendence/delete/{{$attendence->id}}" class="btn btn-danger">Delete</a></td>

                </tr>
               
                @endforeach
            </table>
        </div>


        </div>
    </section>
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
            if(! result){
                evt.stopPropagation();
                evt.preventDefault();   
            }
    }
@endsection
