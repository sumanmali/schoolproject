@extends('adminlayout.app')

@section('content')
<div class="col-sm-9">
	<section class="content-header">
		<h1>
			Teacher
			<small> Add New </small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/home"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li>&nbsp;/ <a href="/teachers"><i class="fa icon-teacher"></i>Teacher</a></li>
			<li class="active">&nbsp;/ Edit </li>
		</ol>
	</section>
	<div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<form novalidate id="entryForm" action="/teacher/edit/{{$teachers->id}}" method="post" enctype="multipart/form-data">
						@csrf
						<div class="box-body">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="name">Name<span class="text-danger">*</span></label>
										<input autofocus type="text" class="form-control" name="name" value="{{$teachers->name}}" value="" required minlength="2" maxlength="255">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="designation">Designation</label>
										<input type="text" class="form-control" name="designation" value="{{$teachers->designation}}" value=""  maxlength="255">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="qualification">Qualification</label>
										<input type="text" class="form-control" name="qualification" value="{{$teachers->qualification}}" value=""  maxlength="255">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="dob">Date of birth<span class="text-danger">*</span></label>
										<input type='date' class="form-control date_picker2" name="dob" value="{{$teachers->dob}}" value="" >
										<span class="fa fa-calendar form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="gender">Gender<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select gender type"></i>
										</label>
										<select class="form-control select2" required="true" name="gender">
											@if($teachers->gender == 1)
											<option value="1" selected="selected">Male</option>
											<option value="2">Female</option>
											@else($teachers->id != 1)
											<option value="2" selected="selected">Female</option>
											<option value="1">Male</option>
											@endif
										</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="bloodgroup">Blood Group<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select blood group type"></i>
										</label>
										<select class="form-control select2" required="true" name="bloodgroup">
											@foreach($bloodgroups as $bgroup)
											@if($bgroup->id == $teachers->bloodgroup)
											<option value="{{$bgroup->id}}">{{$bgroup->name}}</option>
											@endif
											@endforeach

											@foreach($bloodgroups as $bgroup)

											@if($bgroup->id != $teachers->bloodgroup)
											<option value="{{$bgroup->id}}">{{$bgroup->name}}</option>
											@endif
											@endforeach
										</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
							</div>



							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="email">Email<span class="text-danger">*</span></label>
										<input  type="email" class="form-control" name="email"  value="{{$teachers->email}}" value="" maxlength="100" required>
										<span class="fa fa-envelope form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="phone">Phone/Mobile No.<span class="text-danger">*</span></label>
										<input  type="integer" class="form-control" name="phone" required value="{{$teachers->phone}}">
										<span class="fa fa-phone form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="nationality">Nationality<span class="text-danger">*</span></label>
										<input  type="text" class="form-control" name="nationality"  value="{{$teachers->nationality}}" value="" required minlength="4" maxlength="50">
										<span class="fa fa-id-card form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>

							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="joindate">Joining Date<span class="text-danger">*</span></label>
										<input type='date' class="form-control" name="joindate" value="{{$teachers->joindate}}" value="" required/>
										<span class="fa fa-calendar form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="address">Address</label>
										<input type="text" value="{{$teachers->address}}" name="address" class="form-control"  maxlength="500" > </input>
										<span class="fa fa-location-arrow form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="photo" class="imgLabel">Photo</label>
										<input type="file" class="form-control imgUpload" accept=".jpeg, .jpg, .png" name="photo" placeholder="[min 150 X 150 size and max 200kb]">
										<img src="/uploads/{{$teachers->photo}}" alt="{{$teachers->name}}">										
										<span class="glyphicon glyphicon-open-file form-control-feedback" style="top:45px;"></span>
										<span class="text-danger"></span>
									</div>
								</div>

							</div>


						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Update
							</button>
							<button type="reset" class="btn btn-danger btn-sm">
								<i class="fa fa-ban"></i> Reset
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

@endsection