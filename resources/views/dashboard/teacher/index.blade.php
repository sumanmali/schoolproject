  @extends('adminlayout.app')

  @section('content')
  <div class="col-sm-9">
    <section class="content-header">
        <h1>
            Teachers
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="/home">
                    <i class="fa fa-dashboard"></i>&nbsp;Dashboard
                </a>
            </li>
            
            <li class="active">&nbsp;\&nbsp;Teacher</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="/teacher/create"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                            <table id="listDataTableWithSearch" class="table table-bordered table-striped list_view_table display responsive no-wrap" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="25%">Name</th>
                                        <th class="notexport" width="10%">Photo</th>
                                        <th width="8%">ID Card</th>
                                        <th width="15%">Phone No</th>
                                        <th width="19%">Email</th>
                                        <th width="10%">Status</th>
                                        <th class="notexport" width="15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($teachers as $teacher)
                                    <tr>
                                       <td>{{ $loop->iteration }}</td>
                                        <td>{{$teacher->name}}</td>
                                        <td>
                                            <img class="img-responsive center" style="height: 35px; width: 35px;" src="/uploads/{{$teacher->photo}}" alt="{{$teacher->name}}">
                                        </td>
                                        <td>0000{{$teacher->id}}</td>
                                        <td>{{$teacher->phone}}</td>
                                        <td><a href="mailto:{{$teacher->email}}">{{$teacher->email}}</a></td>
                                        <td>
                                            <!-- todo: have problem in mobile device -->
                                            <input class="statusChange" type="checkbox" data-pk="1"  checked  data-toggle="toggle" data-on="<i class='fa fa-check-circle'></i>" data-off="<i class='fa fa-ban'></i>" data-onstyle="success" data-offstyle="danger">
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a title="Edit" href="/teacher/edit/{{$teacher->id}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                            </a>
                                        </div>
                                        <!-- todo: have problem in mobile device -->
                                        <div class="btn-group">
                                            <form method="post" action="/teacher/delete/{{$teacher->id}}">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit" onclick="makeWarning(event)" class="btn btn-danger btn-sm" title="Delete">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach   
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
            if(! result){
                evt.stopPropagation();
                evt.preventDefault();   
            }
    }
</script>
@endsection