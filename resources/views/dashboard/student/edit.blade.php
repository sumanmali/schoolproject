@extends('adminlayout.app')

@section('content')

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/student/edit/{{$students->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger"></span></label>
                                        <input type="text" class="form-control" name="name"  value="{{$students->Student_name}}" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="DOB">Date of birth<span class="text-danger"></span></label>
                                        <input type='date' class="form-control date_picker2"  name="dob"  value="{{$students->DOB}}" required minlength="10" maxlength="255" />
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="gender">Gender<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select gender type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="gender">
                                        @foreach($genders as $gender)
                                            @if($gender->id == $students->gender)
                                                <option value="{{$gender->id}}">{{$gender->name}}</option>
                                            @endif
                                        @endforeach 
                                        @foreach($genders as $gender)
                                            @if($gender->id != $students->gender)
                                            
                                                <option value="{{$gender->id}}">{{$gender->name}}</option>
                                            @endif
                                        @endforeach                                            
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="religion">Religion<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select religion type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="religion" >
                                           @foreach($religions as $religion)
                                            @if($religion->id == $students->religion)
                                                <option value="{{$religion->id}}">{{$religion->name}}</option>
                                            @endif
                                        @endforeach 
                                        @foreach($religions as $religion)
                                            @if($religion->id != $students->religion)
                                            
                                                <option value="{{$religion->id}}">{{$religion->name}}</option>
                                            @endif
                                        @endforeach  

                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="blood_group">Blood Group<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select blood group type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="blood_group" >
                                           @foreach($Blood_Group as $Blood)
                                            @if($Blood->id == $students->Blood)
                                                <option value="{{$Blood->id}}">{{$Blood->name}}</option>
                                            @endif
                                        @endforeach 
                                        @foreach($Blood_Group as $Blood)
                                            @if($Blood->id != $students->Blood)
                                            
                                                <option value="{{$Blood->id}}">{{$Blood->name}}</option>
                                            @endif
                                        @endforeach   
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="nationality">Nationality<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select nationality"></i>
                                        </label>
                                        <select class="form-control" required="true" name="nationality" value="{{$students->Nationality}}">
                                           @if($students->nationality)
                                                <option value="1">others</option>
                                                <option value="0">bangladesh</option>

                                            @else
                                                <option value="0">bangladesh</option>
                                                <option value="1">others</option>

                                           @endif
                                        </select>
                                            <span class="form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                 </div>

                            </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="photo">Photo<span class="text-danger"></span></label>
                                            <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photo"  value="{{$students->photo}}">
                                            <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="email">Email</label>
                                            <input  type="email" class="form-control" name="email"   value="{{$students->Email}}" maxlength="100" >
                                            <span class="fa fa-envelope form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="address">Address</label>
                                            <input  type="text" class="form-control" name="address"   value="{{$students->address}}" maxlength="100" >
                                            <span class="fa fa-envelope form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="phone_no">Phone/Mobile No.</label>
                                            <input  type="text" class="form-control" name="phone_no"  value="{{$students->phone_Number}}" maxlength="15">
                                            <span class="fa fa-phone form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="name"> Father's Name<span class="text-danger"></span></label>
                                            <input type="text" class="form-control" name="fname"  value="{{$students->father_name}}" required minlength="2" maxlength="255">
                                            <span class="fa fa-info form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="name"> Mother's Name<span class="text-danger"></span></label>
                                            <input type="text" class="form-control" name="mname"  value="{{$students->mother_name}}" required minlength="2" maxlength="255">
                                            <span class="fa fa-info form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="fphone_no">  Father's Mobile No.</label>
                                            <input  type="text" class="form-control" name="fphone_no"  value="{{$students->father_phn}}" maxlength="15">
                                            <span class="fa fa-phone form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="mphone_no">  Mother's Mobile No.</label>
                                            <input  type="text" class="form-control" name="mphone_no"  value="{{$students->mother_phn}}" maxlength="15">
                                            <span class="fa fa-phone form-control-feedback"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>

                                </div>  

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type='submit' class='button is-link'>Update</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endsection