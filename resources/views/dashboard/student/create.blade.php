@extends('adminlayout.app')

@section('content')

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/student/create" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Student Info:</p>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="name" value="" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="DOB">Date of birth<span class="text-danger"></span></label>
                                        <input type='date' class="form-control date_picker2"   name="dob" placeholder="date" value="" required minlength="10" maxlength="255" />
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="gender">Gender<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select gender type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="gender">
                                            <option >--------</option>
                                            @foreach($genders as $gender)
                                                    <option value="{{$gender->id}}">{{$gender->name}}</option>
                                                   @endforeach 
                                    </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="religion">Religion<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select religion type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="religion">
                                            <option value="0" selected="selected">--------</option>
                                            @foreach($religions as $religion)
                                           
                                                <option value="{{$religion->id}}">{{$religion->name}}</option>
                                           
                                        @endforeach 
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="blood_group">Blood Group<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select blood group type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="blood_group">
                                            <option>----</option>
                                             @foreach($Blood_Group as $Blood)
                                                <option value="{{$Blood->id}}">{{$Blood->name}}</option>
                                        @endforeach 
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="nationality">Nationality<span class="text-danger"></span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select nationality"></i>
                                        </label>
                                        <select class="form-control" required="true" name="nationality">
                                            <option value="0" selected="selected">-----------</option>
                                            <option value="1" >Bangladeshi</option>
                                            <option value="2">Other</option>
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="photo">Photo<span class="text-danger"></span></label>
                                        <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photo" placeholder="Photo image">
                                        <span class="glyphicon glyphicon-open-file form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="email">Email</label>
                                        <input  type="email" class="form-control" name="email"  placeholder="email address" value="" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="address">Address</label>
                                        <input  type="email" class="form-control" name="address"  placeholder=" address" value="" maxlength="100" >
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="phone_no">Phone/Mobile No.</label>
                                        <input  type="text" class="form-control" name="phone_no" placeholder="phone or mobile number" value="" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name"> Father's Name<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="fname" placeholder="name" value="" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="name"> Mother's Name<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="mname" placeholder="name" value="" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="fphone_no">  Father's Mobile No.</label>
                                        <input  type="text" class="form-control" name="fphone_no" placeholder="phone or mobile number" value="" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="mphone_no">  Mother's Mobile No.</label>
                                        <input  type="text" class="form-control" name="mphone_no" placeholder="phone or mobile number" value="" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>

                            </div>  

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="/student" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-plus-circle "></i>Add </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection