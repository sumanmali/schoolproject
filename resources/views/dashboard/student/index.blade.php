@extends('adminlayout.app')

@section('content')
<div class='col-sm-9'>
<section class="content-header">
        <h1>
            Students
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="/home">
                    <i class="fa fa-dashboard"></i>&nbsp;Dashboard
                </a>
            </li>
            
            <li class="active">&nbsp;\&nbsp;Students</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="/student/create"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Roll N0.</th>
                        <th>ID Card</th>
                        <th>Photo</th>
                        <th width="15%">Phone No.</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Action</th>

                    </thead>
             
                @foreach($students as $student)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{$student->Student_name}}</td>
                    <td>1</td>
                    <td>0123</td>
                    <td><img src="{{ asset('/images/'.$student->photo )}}" style="width:300px;"></td>
                    <td>{{$student->phone_Number}}</td>
                    <td>{{$student->Email}}</td>
                    <td>{{$student->address}}</td>
                    <td><a href="/student/edit/{{$student->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a onclick="makeWarning(event)" href="/student/delete/{{$student->id}}" class="btn btn-danger">Delete</a></td>

                </tr>
               
                @endforeach
            </table>
            </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

</section>
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
            if(! result){
                evt.stopPropagation();
                evt.preventDefault();   
            }
    }
@endsection
