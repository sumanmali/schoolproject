@extends('adminlayout.app')

@section('content')

<div class="col-sm-9">

	<section class="content-header">
		<h1>
			class
			<small> Edit </small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="/class"><i class="fa icon-class"></i> class</a></li>
			<li class="active"> Edit </li>
		</ol>
	</section>
	<div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<form novalidate id="entryForm" action="/class/edit/{{$allClasses->id}}" method="post" enctype="multipart/form-data">
						@csrf
						<div class="box-body">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="name">Class Name<span class="text-danger">*</span></label>
										<input autofocus type="text" class="form-control" name="name" value="{{$allClasses->name}}" required minlength="2" maxlength="10">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="room_id">Choose Room<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select room_id type"></i>
										</label>
										<select class="form-control select2" required="true" name="room_id">
											@foreach($rooms as $room)
											@if($room->id === $allClasses->room_id)
											<option value="{{$room->id}}">{{$room->room_no}}</option>
											@endif
											@endforeach

											@foreach($rooms as $room)

											@if($room->id !== $allClasses->room_id)
											<option value="{{$room->id}}">{{$room->room_no}}</option>
											@endif
											@endforeach
											</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="group_id">Group<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select group_id type"></i>
										</label>
										<select class="form-control select2" required="true" name="group_id">
																					
											@foreach($group as $group1)
											@if($group1->id == $allClasses->group_id)
											<option value="{{$group1->id}}">{{$group1->group_name}}</option>
											@endif
											@endforeach

											@foreach($group as $group2)
											@if($group2->id !== $allClasses->group_id)
											<option value="{{$group2->id}}">{{$group2->group_name}}</option>
											@endif
											@endforeach

											
											</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>							
							</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Submit
							</button>
							<button type="reset" class="btn btn-danger btn-sm">
								<i class="fa fa-ban"></i> Reset
							</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection