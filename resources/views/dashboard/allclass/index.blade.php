  @extends('adminlayout.app')

  @section('content')
  <div class="col-sm-9">
    <section class="content-header">
        <h1>
            Class
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> -> Dashboard</a></li>
            <li class="active"> -> class</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="/class/create"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                            <table id="listDataTableWithSearch" class="table table-bordered table-striped list_view_table display responsive no-wrap" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th class="notexport" width="10%">class Name</th>
                                        <th width="8%">Room</th>
                                        <th width="25%">Group</th>
                                        <th class="notexport" width="15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($allclasses as $class)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        
                                        <td>{{$class->name}}</td>

                                        @foreach($rooms as $room)
                                       @if($class->room_id === $room->id)
                                        <td>{{$room->room_no}}</td>
                                         @endif
                                         @endforeach

                                         @foreach($group as $group1)
                                       @if($class->group_id === $group1->id)
                                        <td>{{$group1->group_name}}</td>
                                         @endif
                                         @endforeach
                                        <td>
                                            <div class="btn-group">
                                                <a title="Edit" href="/class/edit/{{$class->id}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                            </a>
                                        </div>
                                        <!-- todo: have problem in mobile device -->
                                        <div class="btn-group">
                                            <form method="post" action="/class/delete/{{$class->id}}">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit" onclick="makeWarning(event)" class="btn btn-danger btn-sm" title="Delete">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
        if(! result){
            evt.stopPropagation();
            evt.preventDefault();   
        }
    }
</script>
@endsection