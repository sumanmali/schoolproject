@extends('adminlayout.app')

@section('content')

<div class="col-sm-9">

	<section class="content-header">
		<h1>
			Room
			<small> Add New </small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="/room"><i class="fa icon-room"></i> room</a></li>
			<li class="active"> Add </li>
		</ol>
	</section>
	<div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<form novalidate id="entryForm" action="/room/create" method="post" enctype="multipart/form-data">
						@csrf
						<div class="box-body">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="room_no">Room No<span class="text-danger">*</span></label>
										<input autofocus type="text" class="form-control" name="room_no" placeholder="Room Number" required minlength="2" maxlength="10">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="capacity">Capicity</label>
										<input type="text" class="form-control" name="capacity" placeholder="Total capacity for students" value=""  maxlength="255">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="floor">Floor</label>
										<input type="text" class="form-control" name="floor" placeholder="Floor Number" value=""  maxlength="255">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="block">Block<span class="text-danger">*</span></label>
										<input type='text' class="form-control date_picker2" name="block" placeholder="Block Number" value="" >
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>								
							</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Submit
							</button>
							<button type="reset" class="btn btn-danger btn-sm">
								<i class="fa fa-ban"></i> Reset
							</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection