  @extends('adminlayout.app')

  @section('content')
  <div class="col-sm-9">
    <section class="content-header">
        <h1>
            Room
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> >Room</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                            <a class="btn btn-info btn-sm" href="/room/create"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                            <table id="listDataTableWithSearch" class="table table-bordered table-striped list_view_table display responsive no-wrap" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th class="notexport" width="10%">Room No.</th>
                                        <th width="8%">Capacity</th>
                                        <th width="25%">Floor</th>
                                        <th width="8%">Block</th>
                                        <th class="notexport" width="15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rooms as $room)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{$room->room_no}}</td>
                                        <td>{{$room->capacity}}</td>
                                        <td>{{$room->floor}}</td>
                                        <td>{{$room->block}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a title="Edit" href="/room/edit/{{$room->id}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                            </a>
                                        </div>
                                        <!-- todo: have problem in mobile device -->
                                        <div class="btn-group">
                                            <form method="post" action="/room/delete/{{$room->id}}">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit" onclick="makeWarning(event)" class="btn btn-danger btn-sm" title="Delete">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
        if(! result){
            evt.stopPropagation();
            evt.preventDefault();   
        }
    }
</script>
@endsection