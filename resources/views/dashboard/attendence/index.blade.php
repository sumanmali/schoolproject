@extends('adminlayout.app')

@section('content')
<div class='col-sm-9'>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Attendence</h1>
                </div>
            </div>
        </div>
    </div>
    <section class='content'>
        <div class='container-fluid'>
            <p>
                <a href="/attendence/create" class="btn btn-primary">Add new Record</a>
            </p>
            <div class='row'>
                <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Class</th>
                        <th>Student</th>
                        <th>Status</th>
                        <th>Section</th>
                        <th>Action</th>

                    </thead>
             
                @foreach($attendence as $attendence)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    @foreach($allclasses as $class)
                   @if($attendence->class_id === $class->id)
                    <td>{{$class->name}}</td>
                     @endif
                     @endforeach

                     @foreach($students as $student)
                    @if($attendence->student_id == $student->id)
                     <td>{{$student->Student_name}}</td>
                    @endif
                    @endforeach

                    
                  @if($attendence->status == 0)
                    <td>absent</td>
                    @else
                    <td>present</td>
                    @endif

                  @foreach($sections as $section)
                 @if($attendence->section_id == $section->id)
                   <td>{{$section->name}}</td>
                @endif
                @endforeach



                   
            
                    <td><a href="/attendence/edit/{{$attendence->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a href="/attendence/delete/{{$attendence->id}}" class="btn btn-danger">Delete</a></td>

                </tr>
               
                @endforeach
            </table>
        </div>


        </div>
    </section>
</div>

@endsection
