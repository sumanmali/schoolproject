@extends('adminlayout.app')

@section('content')

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/attendence/edit/{{$attendence->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Attendence Info:</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose Class<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="class_id">
                                           @foreach($allclasses as $class)
                                            @if($class->id ==  $attendence->id)
                                            <option value="{{$class->id}}">{{$class->name}}</option>
                                            @endif
                                            @endforeach

                                             @foreach($allclasses as $class)
                                            @if($class->id !==  $attendence->id)
                                            <option value="{{$class->id}}">{{$class->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose student<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="student_id">
                                         
                                            @foreach($students as $student)
                                            @if($student->id == $attendence->id)
                                            <option value="{{$student->id}}">{{$student->Student_name}}</option>
                                            @endif
                                            @endforeach


                                             @foreach($students as $student)
                                            @if($student->id !== $attendence->id)
                                            <option value="{{$student->id}}">{{$student->Student_name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose section_id<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="sections">
                                            
                                            @foreach($sections as $section)
                                            @if($section->id == $attendence->id )
                                            <option value="{{$section->id}}">{{$section->name}}</option>
                                            @endif
                                            @endforeach

                                             @foreach($sections as $section)
                                            @if($section->id !== $attendence->id )
                                            <option value="{{$section->id}}">{{$section->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose status<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="status">
                                           @if($attendence->status)
                                                <option value="1">Present</option>
                                                <option value="0">Absent</option>

                                            @else
                                                <option value="0">Absent</option>
                                                <option value="1">Present</option>

                                           @endif

                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>


                            </div>
                            

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="/student" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection