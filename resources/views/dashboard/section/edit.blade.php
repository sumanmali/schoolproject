@extends('adminlayout.app')

@section('content')

<div class="col-sm-9">

	<section class="content-header">
		<h1>
			Section
			<small> Edit </small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="/class"><i class="fa icon-class"></i> Section</a></li>
			<li class="active"> Edit </li>
		</ol>
	</section>
	<div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<form novalidate id="entryForm" action="/section/create" method="post" enctype="multipart/form-data">
						<div class="box-header">
                            <div class="callout callout-danger">
                                <p><b>Note:</b> Create teacher and Class before creating new Section.</p>
                            </div>
                        </div>
						@csrf
						<div class="box-body">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="name">Section Name<span class="text-danger">*</span></label>
										<input autofocus type="text" class="form-control" name="name" value="{{$sections->name}}" required minlength="2" maxlength="10">
										<span class="fa fa-info form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="class_teacher_id">Choose Teacher<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_teacher_id type"></i>
										</label>
										<select class="form-control select2" required="true" name="class_teacher_id">
										@foreach ($teachers as $teacher)
										@if($teacher -> id === $sections -> class_teacher_id)
											<option value="{{$teacher->id}}">{{$teacher->name}}</option>
										@endif
										@endforeach
										@foreach ($teachers as $teacher)
										@if($teacher -> id !== $sections -> class_teacher_id)
											<option value="{{$teacher->id}}">{{$teacher->name}}</option>
										@endif
										@endforeach
											</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group has-feedback">
										<label for="class_id">Select Class<span class="text-danger">*</span>
											<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
										</label>
										<select class="form-control select2" required="true" name="class_id">
											@foreach($allClasses as $class)
											
																<option value="{{$class->id}}">{{$class->name}}</option>

																@endforeach
											</select>
										<span class="form-control-feedback"></span>
										<span class="text-danger"></span>
									</div>
								</div>							
							</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Submit
							</button>
							<button type="reset" class="btn btn-danger btn-sm">
								<i class="fa fa-ban"></i> Reset
							</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection