@extends('adminlayout.app')

@section('content')
<div class='col-sm-9'>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Exam</h1>
                </div>
            </div>
        </div>
    </div>
    <section class='content'>
        <div class='container-fluid'>
            <p>
                <a href="/exam/create" class="btn btn-primary">Add new Exam</a>
            </p>
            <div class='row'>
                <table class="table table-bordered table-stripped">
                    <thead>
                        <th>S.N</th>
                        <th>Class</th>
                        <th>Name</th>
                        <th>Year</th>
                        <th>Action</th>

                    </thead>
             
                @foreach($exams as $exam)
                <tr>
                    <td>{{ $loop->iteration }}</td>

                    @foreach($allclasses as $class)
                        @if($class->id == $exam->class_id)
                            <td>{{ $class->name }}</td>
                        @endif
                    @endforeach

                    <td>{{$exam->name}}</td>
                    <td>{{$exam->acedemic_year}}</td>
                        
                    <td><a href="/exam/edit/{{$exam->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a onclick="makeWarning(event)" href="/exam/delete/{{$exam->id}}" class="btn btn-danger">Delete</a></td>

                </tr>
               
                @endforeach
            </table>
        </div>


        </div>
    </section>
</div>
<script type="text/javascript">
    function makeWarning(evt){
        let result = confirm("Are you sure to Delete?");
            if(! result){
                evt.stopPropagation();
                evt.preventDefault();   
            }
    }
</script>
@endsection
