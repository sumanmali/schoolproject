@extends('adminlayout.app')

@section('content')

<div class='col-sm-9'>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form action="/exam/create" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <p class="lead section-title">Exam Info:</p>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="class_id">Choose Class<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="select class_id type"></i>
                                        </label>
                                        <select class="form-control select2" required="true" name="class_id">
                                            <option>-----</option>
                                            @foreach($allclasses as $class)
                                            <option value="{{$class->id}}">{{$class->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name<span class="text-danger"></span></label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="name" value="" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="year">Academic_year</label>
                                        <input type="text" class="form-control" name="acedemic_year" placeholder="year" value=""  maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                


                            </div>
                            

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="/student" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa fa-plus-circle "></i>Add </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection