<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
      <div class="profile-userpic">
        <img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
      </div>
      <div class="clear"></div>
    </div>
    <div class="divider"></div>
    <form role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Search">
      </div>
    </form>
     <ul style="display: block;" class="nav menu">
      <li><a href="/home"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
      <li><a href="/teacher"><em class="fa fa-calendar">&nbsp;</em> Teacher</a></li>
      <li><a href="/student"><em class="fa fa-bar-chart">&nbsp;</em> Student</a></li>
      <li><a href="/room"><em class="fa fa-bar-chart">&nbsp;</em> room</a></li>
      <li><a href="/class"><em class="fa fa-bar-chart">&nbsp;</em> class</a></li>
      <li><a href="/staff"><em class="fa fa-toggle-off">&nbsp;</em> Staff</a></li>
      <li><a href="/section"><em class="fa fa-bar-chart">&nbsp;</em> section</a></li>
      <li><a href="/subjects"><em class="fa fa-bar-chart">&nbsp;</em> subject</a></li>
      <li><a href="/guardian"><em class="fa fa-toggle-off">&nbsp;</em> Guardian</a></li>
      <li><a href="/parent"><em class="fa fa-clone">&nbsp;</em> Parent</a></li>
      <li><a href="/attendence"><em class="fa fa-clone">&nbsp;</em> Attendence</a></li>
      <li><a href="/teacher-attendence"><em class="fa fa-clone">&nbsp;</em> Teacher Attendence</a></li>
      <li><a href="/staff-attendence"><em class="fa fa-clone">&nbsp;</em> Staff Attendence</a></li>
      <li><a href="/exam"><em class="fa fa-clone">&nbsp;</em> Exam</a></li> 
      <li><a href="/mark"><em class="fa fa-clone">&nbsp;</em> Marks</a></li>
      <li><a href="/result"><em class="fa fa-clone">&nbsp;</em> Results</a></li>
      <li class="parent ">
        <a data-toggle="collapse" href="#">
          <em class="fa fa-sliders" aria-hidden="true">&nbsp;</em> Slider
          <span data-toggle="collapse"  class="icon pull-right"></span>
        </a>
      </li>
        <li> <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <em class="fa fa-circle-o text-red"></em>
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>

      
     
    </ul>
  </div>