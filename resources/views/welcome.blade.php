<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            div#userLoginModal {
                border: 1px solid #efefef;
                box-shadow: 0px 0px 15px 0px #efefef;
                width: 12rem;
                height: auto;
                right: 85px;
                top: 50px;
                transition: .5s ease-in;
                background: white;
                position: absolute;
            }
            .modalContent .header {
                border-bottom: 1px solid #efefef;
                padding: 10px 25px;
            }

            .modalContent .header span.close {
                float: right;
                font-weight: 700;
                cursor: pointer;
                font-size: 17px;
            }
            .modalContent .header h4 {
                display: inline-block;
                margin: 0px;
                font-size: 14px;
                font-weight: 600;
            }
            .modalContent .inner-content ul {
                list-style: none;
            }
            .modalContent .inner-content ul {
                list-style: none;
                margin: 0px;
                padding: 0px;
            }
            .modalContent .inner-content ul li button {
                border: 1px solid;
            }
            .modalContent .inner-content ul li {
                padding: 6px 17px;
            }
            .modalContent .inner-content {
                padding: 8px 10px 14px;
            }
            .modalContent button.btn.btn-block:focus {
                box-shadow: none;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}" onclick="loginAs(event)">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

    

            <div  id="userLoginModal" style="display: none">
                <div class="modalContent">
                    <div class="header">
                        <h4>Login As</h4>
                        <span class="close">&times;</span>
                    </div>
                    <div class="inner-content">
                        <ul>
                            <li>
                                <button  class="btn btn-block btn-primary btn-loginas">Adminstration</button>
                            </li>
                            <li>
                                <button  class="btn btn-block btn-info btn-loginas">Teacher</button>
                            </li>
                            <li>
                                <button  class="btn btn-block btn-success btn-loginas">Parent</button>
                            </li>
                            <li>
                                <button  class="btn btn-block btn-secondary btn-loginas">Student</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
        <script>
            function loginAs(evt){

                evt.preventDefault();

                var modal = document.getElementById('userLoginModal');
                modal.style.display = "block";
                var close = document.getElementsByClassName('close')[0];
                close.addEventListener('click', function(evt){
                    modal.style.display = "none";
                });
                var x = `<?php echo route('login') ?>`;

                var buttons = document.getElementsByClassName('btn-loginas');
                buttons[0].addEventListener('click', function(){
                    sessionStorage.setItem('loginas', 'admin');
                    redirect(x);
                });

                buttons[1].addEventListener('click', function(){
                    sessionStorage.setItem('loginas', 'teacher');
                    redirect(x);
                });

                buttons[2].addEventListener('click', function(){
                    sessionStorage.setItem('loginas', 'parent');
                    redirect(x);
                });

                buttons[3].addEventListener('click', function(){
                    sessionStorage.setItem('loginas', 'student');
                    redirect(x);
                });

            }

            function redirect(x){
                window.location.href=x;                

            }
        </script>
    </body>
</html>
