Device Model                                     iPhone 6                                                                iPhone 6                                             Normal
Device Color                                     Front White��Rear Silver                                                Front White��Rear Silver                             Normal
Hard Disk Capacity                               16GB                                                                    16GB Hynix MLC                                       Normal
Sales Model                                      MG482                                                                   MG482                                                Normal
Sales Area                                       J/A                                                                     J/A                                                  Normal
Model Number                                     A1586                                                                   A1586                                                Normal
Buletooth Address                                CC:29:F5:1F:4F:13                                                       CC:29:F5:1F:4F:13                                    Normal
Cellular Address                                 CC:29:F5:1F:4E:A7                                                       CC:29:F5:1F:4E:A7                                    Normal
Wi-Fi Address                                    CC:29:F5:1F:4E:A6                                                       CC:29:F5:1F:4E:A6                                    Normal
Serial Number                                    F73PC6P8G5MP 2015-03-08                                                 F73PC6P8G5MP 2015-03-08                              Normal
Mainboard Serial No.                             C7H5094290AF98FAU 2015-02-26                                            C7H5094290AF98FAU 2015-02-26                         Normal
Battery Serial Number                            FG95092Q02FFW60B5 2015-02-24                                            FG96402A1GAFW60EL 2016-09-27                         Battery may be changed
Rear Camera                                      DN850736RNNFNM5DB 2015-02-11                                            DN850736RNNFNM5DB 2015-02-11                         Normal
Front Camera                                     F0W50722R5XFG1P4D 2015-02-10                                            F0W50722R5XFG1P4D 2015-02-10                         Normal
Screen Serial Number                             C3F5093K830FT37L-A2J6J3535FP31-G60620515077F1A1B LG 2015-02-25          User decision needed                                 N/A
TouchID Serial Number                            04B40E06180B7901F501D4915F57BC4A 2015-01-29                             04B40E06180B7901F501D4915F57BC4A 2015-01-29          Normal

Production Date 2015/03/08 (10th Week )          Warranty Date N/A                                                       Apple ID Lock Off
iOS Version 12.4 (16G5038d)                      Activated Yes                                                           Jailbroken No
Battery Life 90%                                 Charge Times 666 Times                                                  Wi-Fi Module High Temp Wi-Fi
Carrier Status Load failed                       Verify Serial Number Yes                                                Verify UDID Yes

Verification completed, the device was disassembled Report Date 2019-06-14